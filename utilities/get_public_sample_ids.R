require(tidyverse)

source("/published_data_archive/paper_data/paper_data_RR335/code/utilities/get_filepaths.R")
filepaths <- get_filepaths()

pan_cancer_public_sample_id_mappings <- read_tsv(paste0(filepaths$filepath_data, "pan_cancer_public_sample_id_mappings.tsv"),
                                                 col_types = list(col_character(),
                                                                  col_character(),
                                                                  col_character()))

get_public_sample_ids_remove_participant_id_and_tumour_sample_platekey <- function(data){
  data <- left_join(data, pan_cancer_public_sample_id_mappings,
                    by = c("participant_id" = "participant_id",
                           "tumour_sample_platekey" = "tumour_sample_platekey")) %>% 
    select(-c(participant_id, tumour_sample_platekey)) %>% 
    filter(!is.na(public_sample_id))
  return(data)
}

get_public_sample_ids_remove_participant_id <- function(data){
  data <- left_join(data, pan_cancer_public_sample_id_mappings,
                    by = c("participant_id" = "participant_id"),
                    #may need relationship argument to avoid many-to-many warning in newer versions of dplyr (>=1.1.1)
                    #many-to-many should not matter as there is downstream filtering using dates
                    relationship = "many-to-many") %>% 
    select(-c(participant_id)) %>% 
    filter(!is.na(public_sample_id))
  return(data)
}