require(tidyverse)

get_master_table <- function(){
  column_types <- list(
    col_character(), #public_sample_id
    col_character(), #pan_cancer_code
    col_character(), #pan_cancer_code_definition
    col_character(), #sex
    col_character(), #library_type
    col_double(),    #age
    col_character(), #preparation_method
    col_date(),      #tumour_clinical_sample_time
    col_character(), #sample_type
    col_double(),    #tumour_purity
    col_character(), #tissue_source
    col_factor(),    #tissue_source_reduced
    col_double(),    #somatic_coding_variants_per_mb
    col_character(), #cancer_stage
    col_double(),    #status
    col_character(), #icd10_underlying_cause
    col_double(),    #survival data
    col_character(), #survival data
    col_character(), #survival data
    col_character(), #survival data
    col_double(),    #ploidy
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #SBS signature
    col_double(),    #signature aetiology
    col_double(),    #signature aetiology
    col_double(),    #signature aetiology
    col_double(),    #signature aetiology
    col_double(),    #signature aetiology
    col_double(),    #signature aetiology
    col_double(),    #signature aetiology
    col_double(),    #chord data
    col_character(), #chord data
    col_character(), #chord data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small somatic variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #small germline variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #structural variant data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character(), #copy number aberration data
    col_character()  #copy number aberration data
  )
  
  source("/published_data_archive/paper_data/paper_data_RR335/code/utilities/get_filepaths.R")
  filepaths <- get_filepaths()
  
  pancan_data_plus_vardata_variants <- read_tsv(paste0(filepaths$filepath_master_table, "pan_cancer_master_table_2023-08-30.tsv"),
                                                col_names = TRUE,
                                                col_types = column_types)
  return(pancan_data_plus_vardata_variants)
}