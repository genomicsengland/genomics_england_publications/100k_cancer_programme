require(tidyverse)

get_actionability_plot <- function(vardata_counts,
                                   colour_present = "darkorange",
                                   colour_not_present = "dodgerblue2",
                                   alpha_zero = 0,
                                   alpha_superlow = 0.075,
                                   alpha_low = 0.15,
                                   alpha_lowmid = 0.30,
                                   alpha_mid = 0.55,
                                   alpha_midhigh = 0.75,
                                   alpha_high = 1,
                                   hjust_value = 0,
                                   coord_flip = TRUE){
  plot <- ggplot(vardata_counts, aes(pan_cancer_code_definition, targets)) + 
    geom_tile(aes(fill = test_scope_percent_bin, alpha = test_scope_percent_bin), col = "orange") + 
    geom_text(aes(label = case_when(percent == 0 ~ "",
                                    percent < 1 ~ "<1",
                                    percent >= 1 ~ as.character(percent)), colour = test_scope), size = 2.5) + 
  scale_fill_manual(labels = c(rep("Present in test directory for disease", 6),
                               rep("Not present in test directory for disease", 6)),
                    values = c(gene_present_in_test_directory_for_disease_zero = colour_present,
                               gene_present_in_test_directory_for_disease_low = colour_present,
                               gene_present_in_test_directory_for_disease_lowmid = colour_present,
                               gene_present_in_test_directory_for_disease_mid = colour_present,
                               gene_present_in_test_directory_for_disease_midhigh = colour_present,
                               gene_present_in_test_directory_for_disease_high = colour_present,
                               gene_not_present_in_test_directory_for_disease_zero = colour_not_present,
                               gene_not_present_in_test_directory_for_disease_low = colour_not_present,
                               gene_not_present_in_test_directory_for_disease_lowmid = colour_not_present,
                               gene_not_present_in_test_directory_for_disease_mid = colour_not_present,
                               gene_not_present_in_test_directory_for_disease_midhigh = colour_not_present,
                               gene_not_present_in_test_directory_for_disease_high = colour_not_present)) + 
  scale_alpha_manual(values = c(gene_present_in_test_directory_for_disease_zero = alpha_superlow,
                                gene_present_in_test_directory_for_disease_low = alpha_low,
                                gene_present_in_test_directory_for_disease_lowmid = alpha_lowmid,
                                gene_present_in_test_directory_for_disease_mid = alpha_mid,
                                gene_present_in_test_directory_for_disease_midhigh = alpha_midhigh,
                                gene_present_in_test_directory_for_disease_high = alpha_high,
                                gene_not_present_in_test_directory_for_disease_zero = alpha_zero,
                                gene_not_present_in_test_directory_for_disease_low = alpha_low,
                                gene_not_present_in_test_directory_for_disease_lowmid = alpha_lowmid,
                                gene_not_present_in_test_directory_for_disease_mid = alpha_mid,
                                gene_not_present_in_test_directory_for_disease_midhigh = alpha_midhigh,
                                gene_not_present_in_test_directory_for_disease_high = alpha_high)) + 
  scale_colour_manual(values = c(gene_present_in_test_directory_for_disease = "black",  gene_not_present_in_test_directory_for_disease = "black"), guide = "none") +
  theme(panel.background = element_rect(fill = "white"),
        axis.text = element_text(size = 6.5, colour = "black"),
        axis.text.x = element_text(angle = 270, hjust = hjust_value, vjust = 0.5, size = 9),
        axis.text.y = element_text(face = c("italic")),
        axis.ticks = element_blank(),
        axis.title.y = element_blank(),
        axis.title.x = element_blank(),
        legend.position = "right")
if(coord_flip){
  print("flipping plot coords.")
  plot <- plot + coord_flip()
}
return(plot)
}