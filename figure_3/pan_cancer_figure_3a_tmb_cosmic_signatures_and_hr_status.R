rm(list=ls())
library(tidyverse)
library(lubridate)
library(grid)
library(gridExtra)
library(gtable)
library(RColorBrewer)
library(svglite)

source("/published_data_archive/paper_data/paper_data_RR335/code/utilities/get_filepaths.R")
filepaths <- get_filepaths()

source(paste0(filepaths$filepath_code, "get_master_table.R"))
pancan_data_plus_vardata_variants <- get_master_table()

pancan_data_plus_vardata_variants_filtered <- pancan_data_plus_vardata_variants %>% 
  group_by(pan_cancer_code_definition) %>% 
  mutate(ci_count = n()) %>% 
  filter(ci_count >= 40) %>% 
  ungroup()

#this is required to fetch random_downsample_tmb (a random downsample to 100 tumours per disease type*)
#*certain disease types have <100 tumours and so are excluded from this version of the figure
random_downsample_tmb <- read_tsv(paste0(filepaths$filepath_data, "random_downsample_tmb_order_with_public_sample_ids.tsv"))

random_downsample_tmb <- pancan_data_plus_vardata_variants_filtered %>% 
  left_join(., random_downsample_tmb, by = c("public_sample_id" = "public_sample_id")) %>% 
  filter(!is.na(index)) %>% 
  arrange(median, index) %>% 
  mutate(sig_other_v3 = 100 - (sig_mmr_v3 + 
                                  sig_pole_v3 + 
                                  sig_aging_v3 + 
                                  sig_hr_v3 + 
                                  sig_uv_v3 + 
                                  sig_tobacco_v3 + 
                                  sig_apobec_v3))

plot_theme <- theme(axis.line.x = element_blank(),
                    axis.title.x = element_blank(),
                    axis.text.x = element_blank(),
                    axis.ticks.x = element_blank(),
                    axis.line.y = element_blank(),
                    axis.title.y = element_blank(),
                    axis.text.y = element_blank(),
                    axis.ticks = element_blank(),
                    panel.grid = element_blank(),
                    panel.background = element_rect(fill = "white", colour = "grey50"))

plotting_tmb <- function(data, disease){
  data <- data %>% filter(pan_cancer_code_definition == disease)
  plot <- ggplot(data) + 
    geom_point(aes(index, somatic_coding_variants_per_mb), size = 1.75, alpha = 0.75) + 
    stat_summary(aes(mean_index, somatic_coding_variants_per_mb), fun = median, fun.min = median, fun.max = median,
                 geom = "crossbar", colour = "red", width = 100, size = 0.35) + 
    scale_y_log10(limits = c(0.01, 2000), breaks = c(0.01, 0.1, 1, 10, 100, 1000)) + 
    plot_theme + 
    theme(panel.grid.major.y = element_line(colour = "grey80"))
  return(plot)
}

plotting_sigs_sbs <- function(data, disease, legend_position){
  palette <- brewer.pal(9, "Paired")
  #this adds in grey and orange for sig_other and sig_apobec respectively - depends on factor ordering below
  palette <- c("Other" = "grey96",
               "Aging" = palette[3],
               "Apobec" = palette[7],
               "MMR" = "dodgerblue1",
               "Pol-E" = "navy",
               "HR" = palette[4],
               "UV" = palette[5],
               "Tobacco" = palette[6])
  data <- data %>% 
    filter(pan_cancer_code_definition == disease)
  data <- data %>% 
    select(index,
           sig_mmr_v3,
           sig_pole_v3,
           sig_aging_v3,
           sig_hr_v3,
           sig_uv_v3,
           sig_tobacco_v3,
           sig_apobec_v3,
           sig_other_v3) %>% 
    gather(key = "Signature", value = "percentage", starts_with("sig_")) %>% 
    mutate(Signature = case_when(Signature == "sig_other_v3" ~ "Other",
                                 Signature == "sig_mmr_v3" ~ "MMR",
                                 Signature == "sig_pole_v3" ~ "Pol-E",
                                 Signature == "sig_aging_v3" ~ "Aging",
                                 Signature == "sig_hr_v3" ~ "HR",
                                 Signature == "sig_uv_v3" ~ "UV",
                                 Signature == "sig_tobacco_v3" ~ "Tobacco",
                                 Signature == "sig_apobec_v3" ~ "Apobec")) %>% 
    mutate(Signature = factor(Signature, levels = c("Other","Aging","Apobec","MMR","Pol-E","HR","UV","Tobacco")))
  plot <- ggplot(data, aes(index, percentage, fill = Signature, col = Signature)) +
    geom_bar(stat = "identity") +
    scale_colour_manual(values = palette) +
    scale_fill_manual(values = palette) + 
    plot_theme + 
    theme(legend.position = legend_position)
  return(plot)
}

plotting_chord <- function(data, disease, legend_position){
  palette <- c("HR deficient" = "magenta", "HR proficient" = "skyblue", "NA" = "grey95")
  data <- data %>% 
    filter(pan_cancer_code_definition == disease)
  data <- data %>% 
    select(index, chord_hr_status) %>% 
    mutate(percentage = 1) %>% 
    mutate(`HR status` = case_when(chord_hr_status == "HR_deficient" ~ "HR deficient",
                                   chord_hr_status == "HR_proficient" ~ "HR proficient",
                                   chord_hr_status == "cannot_be_determined" ~ "NA"),
           `HR status` = factor(`HR status`, levels = c("HR deficient", "HR proficient", "NA")))
  plot <- ggplot(data, aes(index, percentage, fill = `HR status`, col = `HR status`)) +
    geom_bar(stat = "identity") +
    scale_colour_manual(values = palette) +
    scale_fill_manual(values = palette) +  
    plot_theme + 
    theme(legend.position = legend_position)
  return(plot)
}

plots_list_tmb <- list()
plots_list_sigs_sbs <- list()
plots_list_chord <- list()
for(i in unique(random_downsample_tmb$pan_cancer_code_definition)){
  plots_list_tmb[[i]] <- plotting_tmb(random_downsample_tmb, i)
  plots_list_sigs_sbs[[i]] <- plotting_sigs_sbs(random_downsample_tmb, i, "none")
  plots_list_chord[[i]] <- plotting_chord(random_downsample_tmb, i, "none")
}

p_legend_plot_sbs <- plotting_sigs_sbs(random_downsample_tmb, i, "bottom")
#using specific disease to make sure we have full legend
p_legend_plot_chord <- plotting_chord(random_downsample_tmb, "Lung Squamous Cell Carcinoma", "bottom")
p_legend_sbs <- gtable_filter(ggplot_gtable(ggplot_build(p_legend_plot_sbs)), "guide-box")
p_legend_chord <- gtable_filter(ggplot_gtable(ggplot_build(p_legend_plot_chord)), "guide-box")

plots_tmb <- c(plots_list_tmb)
plots_sigs_sbs <- c(plots_list_sigs_sbs)
plots_sigs_chord <- c(plots_list_chord)

num_plots <- length(plots_sigs_sbs)
cols <- num_plots
layout <- matrix(seq(1, cols * ceiling(num_plots/cols)), ncol = cols, nrow = ceiling(num_plots/cols))

lower_scale <- 0
upper_scale <- 10

y_val_vp1 <- 0.70
height_val_vp1 <- 0.29
y_val_vp2 <- 0.40
height_val_vp2 <- 0.29

svglite(file = paste0(filepaths$filepath_plots, "pan_cancer_figure_3a_tmb_cosmic_signatures_and_hr_status_", Sys.Date(),".svg"), height = 12, width = 60)
grid.newpage()

vp1 <- viewport(x = unit(0.02, "npc"), y = unit(0.70, "npc"),
                width = unit(0.9, "npc"), height = unit(0.29, "npc"), just = c("left", "bottom"),
                layout = grid.layout(nrow(layout), ncol(layout)))
pushViewport(vp1)
grid.rect(gp = gpar(lty = "solid"))
grid.yaxis(at = seq(0.083,0.9,0.801/5), label = c("0.01", "0.1","1","10","100","1000"), gp = gpar(fontsize = 18))
for (i in 1:num_plots) {
  # Get the i,j matrix positions of the regions that contain this subplot
  matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
  
  print(plots_tmb[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                      layout.pos.col = matchidx$col))
}
upViewport()

#sbs
vp2 <- viewport(x = unit(0.02, "npc"), y = unit(0.40, "npc"),
                width = unit(0.9, "npc"), height = unit(0.29, "npc"), just = c("left", "bottom"),
                layout = grid.layout(nrow(layout), ncol(layout)))
pushViewport(vp2)
grid.rect(gp = gpar(lty = "solid", col = "white"))
grid.yaxis(at = seq(0.082,1,0.212), label = c("0", "25","50","75","100"), gp = gpar(fontsize = 18))
for (i in 1:num_plots) {
  # Get the i,j matrix positions of the regions that contain this subplot
  matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
  
  print(plots_sigs_sbs[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                           layout.pos.col = matchidx$col))
}
upViewport()

#chord
vp3 <- viewport(x = unit(0.02, "npc"), y = unit(0.35, "npc"),
                width = unit(0.9, "npc"), height = unit(0.05, "npc"), just = c("left", "bottom"),
                layout = grid.layout(nrow(layout), ncol(layout)))
pushViewport(vp3)
for (i in 1:num_plots) {
  # Get the i,j matrix positions of the regions that contain this subplot
  matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
  print(plots_sigs_chord[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                             layout.pos.col = matchidx$col))
}
upViewport()

#create viewport for the labels below the plot viewport
vp4 <- viewport(x = unit(0.02, "npc"), y = unit(0.58, "npc"),
                width = unit(0.9, "npc"), height = unit(0.5, "npc"), just = c("left","top"),
                layout = grid.layout(nrow(layout), ncol(layout)))
pushViewport(vp4)
for (i in 1:num_plots) {
  # Get the i,j matrix positions of the regions that contain this subplot
  matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
  print(grid.text(names(plots_sigs_sbs[i]),
                  gp = gpar(fontsize = 14),
                  rot = 270,
                  hjust = 0, vp = viewport(layout.pos.row = matchidx$row,
                                           layout.pos.col = matchidx$col)))
}
upViewport()

#create viewports for the legends
vp5 <- viewport(x = unit(0.38, "npc"), y = unit(0.05, "npc"),
                width = unit(0.5, "npc"), height = unit(0.1, "npc"), just = c("centre","bottom"))
pushViewport(vp5)
grid.draw(p_legend_sbs)
upViewport()

vp6 <- viewport(x = unit(0.38, "npc"), y = unit(0.00, "npc"),
                width = unit(0.5, "npc"), height = unit(0.1, "npc"), just = c("centre","bottom"))
pushViewport(vp6)
grid.draw(p_legend_chord)
upViewport()

dev.off()
