# 100k_cancer_programme

This code consists of scripts which, in conjunction with data available within the Genomics England Research Environment (https://www.genomicsengland.co.uk/research/academic/join-gecip), allows registered users to recreate figures from the "Insights for Precision Oncology from the 100,000 Genomes Cancer Programme" paper by Sosinsky et al. 2023. Further details on the precise location of the data within the Research Environment can be found at https://re-docs.genomicsengland.co.uk/pan_cancer_pub/

The code runs with R 4.2.1 in the Genomics England Research Environment (i.e. the code relies on libraries installed in that version of R). Users will likely need to install some additional packages themselves, and can follow instructions at https://re-docs.genomicsengland.co.uk/r_packages/ to do so.

Code organisation:
- figure_1/
  - pan_cancer_figure_1e_stage_vs_disease_type.R
  - pan_cancer_figure_1e_treatment_status_vs_disease_type.R
  - pan_cancer_figure_1e_tumour_purity_vs_disease_type.R

- figure_2/
  - pan_cancer_figure_2_small_variants_somatic.R
  - pan_cancer_figure_2_copy_number_aberrations.R
  - pan_cancer_figure_2_structural_variants.R
  - pan_cancer_figure_2_hrd.R
  - pan_cancer_figure_2_tmb.R
  - pan_cancer_figure_2_mmr_signatures.R
  - pan_cancer_figure_2_small_variants_germline.R
  - pan_cancer_figure_2_pharmacogenomic_variants.R

- figure_3/
  - pan_cancer_figure_3a_tmb_cosmic_signatures_and_hr_status.R
  - pan_cancer_figure_3b_survival_curves_hr_and_mmr_status.R
  - pan_cancer_figure_3c_survival_curves_skcm_and_luad.R

- figure_4/
  - pan_cancer_figure_4a_small_variants_and_copy_number_aberrations.R
  - pan_cancer_figure_4b_survival_curves_gene_alterations.R

- utilities/
  - get_actionability_plot.R
  - get_actionable_targets.R
  - get_filepaths.R
  - get_master_table.R
  - get_pan_cancer_code_order.R
  - get_public_sample_ids.R
  - get_pan_cancer_code_order.R
  - get_treatments.R
  - get_vardata_counts_splits.R
