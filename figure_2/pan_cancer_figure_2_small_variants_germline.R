rm(list=ls())
library(tidyverse)
library(grid)
library(gridExtra)
library(gtable)
library(svglite)

source("/published_data_archive/paper_data/paper_data_RR335/code/utilities/get_filepaths.R")
filepaths <- get_filepaths()

source(paste0(filepaths$filepath_code, "get_master_table.R"))
pancan_data_plus_vardata_variants <- get_master_table()

###################
#get actionable targets
test_scope_list <- c("Exon level CNVs", "Small variants", "CNVs")
source(paste0(filepaths$filepath_code, "get_actionable_targets.R"))
actionable_small_variant_germline_targets <- get_actionable_germline_variant_targets_small_variants(test_scope_list)

#next bit is trying to set up a dataframe to allow all genes in the overall gene list to be joined, per disease type, to the actual counts...
#...and where that gene is not meant to be in the list for that disease type, it will be greyed out (that happens further down)
actionable_small_variant_germline_targets_total_list <- get_reformatted_target_list(pancan_data_plus_vardata_variants, actionable_small_variant_germline_targets)
###################

vardata_counts <- pancan_data_plus_vardata_variants %>% 
  select(public_sample_id, pan_cancer_code, starts_with("small_ger.")) %>% 
  pivot_longer(cols = starts_with("small_ger."),
               names_to = "gene",
               values_to = "small_var_type") %>% 
  separate_rows(small_var_type, sep = ",") %>% 
  mutate(gene = str_remove_all(gene, "small_ger.")) %>% 
  filter(complete.cases(.)) %>% 
  count(public_sample_id, pan_cancer_code, gene) %>% 
  count(pan_cancer_code, gene) %>% 
  arrange(-n) %>% 
  left_join(actionable_small_variant_germline_targets_total_list, ., by = c("pan_cancer_code" = "pan_cancer_code", "targets" = "gene")) %>% 
  left_join(., pancan_data_plus_vardata_variants %>% count(pan_cancer_code), by = c("pan_cancer_code" = "pan_cancer_code")) %>%
  mutate(percent = n.x/n.y*100) %>% 
  mutate(percent = case_when(is.na(percent) ~ 0,
                             percent <= 0.5 ~ percent,
                             percent > 0.5 ~ round(percent, 0)),
         n.x = ifelse(is.na(n.x), 0, n.x)) %>% 
  mutate(test_scope = factor(test_scope,
                             levels = c("gene_present_in_test_directory_for_disease",
                                        "gene_not_present_in_test_directory_for_disease"))) %>% 
  left_join(., pancan_data_plus_vardata_variants %>% 
              select(pan_cancer_code, pan_cancer_code_definition) %>% 
              unique(),
            by = c("pan_cancer_code" = "pan_cancer_code"))

source(paste0(filepaths$filepath_code, "get_pan_cancer_code_order.R"))
vardata_counts_levels <- get_vardata_counts_levels()
vardata_counts <- vardata_counts %>% slice(which(vardata_counts$pan_cancer_code_definition %in% vardata_counts_levels))
vardata_counts$pan_cancer_code_definition <- factor(vardata_counts$pan_cancer_code_definition,
                                                    levels = rev(vardata_counts_levels))

factor_order <- vardata_counts %>% 
  select(targets, pan_cancer_code_definition, n.x) %>% 
  unique() %>% 
  pivot_wider(names_from = "pan_cancer_code_definition", values_from = "n.x", values_fill = 0) %>% 
  mutate(row_sum = rowSums(select(., -"targets")),
         row_sum_norm = rowSums(select(., -"targets"))/sum((pancan_data_plus_vardata_variants %>% count(pan_cancer_code))$n)) %>% 
  filter(row_sum > 0) %>% 
  arrange(row_sum_norm)

#remove genes with no variants
vardata_counts <- vardata_counts %>% slice(which(vardata_counts$targets %in% factor_order$targets))

#add this order to the target column
factor_order <- factor_order %>% 
  mutate(targets = factor(targets, levels = rev(factor_order$targets)))

#reorder genes by overall number of variants found (normalised)
vardata_counts$targets <- factor(vardata_counts$targets, levels = factor_order$targets)

source(paste0(filepaths$filepath_code, "get_vardata_counts_splits.R"))
vardata_counts <- get_vardata_counts_splits(vardata_counts)

source(paste0(filepaths$filepath_code, "get_actionability_plot.R"))
plot <- get_actionability_plot(vardata_counts, coord_flip = FALSE, hjust_value = 0)
plot <- plot + theme(axis.text.x = element_text(angle = 90))

#grab legend from plot...
p_legend <- gtable_filter(ggplot_gtable(ggplot_build(plot)), "guide-box")
#grab x-axis from plot...
p_xaxis <- gtable_filter(ggplot_gtable(ggplot_build(plot)), "axis-b")
#...then remove legend from plot
plot <- plot + theme(legend.position = "none",
                     axis.text.x = element_blank())

count_labels_plot <- ggplot() + 
  geom_text(data = vardata_counts %>% select(pan_cancer_code_definition, n.y) %>% unique(), aes(x = pan_cancer_code_definition, y = 0.25, label = n.y),
            inherit.aes = FALSE,
            size = 2.5,
            hjust = 1) + 
  coord_flip()
count_labels <- gtable_filter(ggplot_gtable(ggplot_build(count_labels_plot)), "panel")$grob[[1]]$children[[3]]

barplot <- ggplot(factor_order, aes(rev(targets), row_sum_norm*100)) +
  geom_bar(stat = "identity", fill = "grey80") +
  scale_y_continuous(expand = c(0, 0), limits = c(0, 1), position = "right") +
  ylab("% of tumours (pan cancer) with\n1+ variant in gene") +
  coord_flip() +
  theme(panel.background = element_rect(fill = "white"),
        axis.text.y = element_blank(),
        axis.text.x = element_text(size = 6.5),
        axis.title.y = element_blank(),
        axis.title.x = element_text(size = 8),
        axis.ticks.y = element_blank(),
        axis.ticks.x = element_line(size = 0.25),
        axis.line = element_line(color = "black", size = 0.25),
        legend.position = "none")

svglite(file = paste0(filepaths$filepath_plots, "pan_cancer_figure_2_small_variants_germline_", Sys.Date(),".svg"), height = 12, width = 12)
grid.newpage()
vp1 <- viewport(x = unit(0.0, "npc"), y = unit(0.05, "npc"), width = unit(0.6, "npc"), height = unit(0.60, "npc"), just = c("left", "bottom"))
pushViewport(vp1)
grid.draw(ggplotGrob(plot))
popViewport()

vp2 <- viewport(x = unit(0.60, "npc"), y = unit(0.055, "npc"), width = unit(0.15, "npc"), height = unit(0.630, "npc"), just = c("left", "bottom"))
pushViewport(vp2)
grid.draw(ggplotGrob(barplot))
popViewport()

#x-axis viewport
vp3 <- viewport(x = unit(0.044, "npc"), y = unit(0.74, "npc"), width = unit(0.549, "npc"), height = unit(0.05, "npc"), just = c("left", "bottom"))
pushViewport(vp3)
grid.draw(p_xaxis)
popViewport()

dev.off()
