rm(list=ls())
library(tidyverse)
library(grid)
library(gridExtra)
library(gtable)
library(svglite)

source("/published_data_archive/paper_data/paper_data_RR335/code/utilities/get_filepaths.R")
filepaths <- get_filepaths()

source(paste0(filepaths$filepath_code, "get_master_table.R"))
pancan_data_plus_vardata_variants <- get_master_table()

#next bit is trying to set up a dataframe to allow all genes in the overall gene list to be joined, per disease type, to the actual counts...
#...and where that gene is not meant to be in the list for that disease type, it will be greyed out (that happens further down)
source(paste0(filepaths$filepath_code, "get_actionable_targets.R"))
actionable_tmb_list <- data.frame(targets = "tmb",
                                  pan_cancer_code = "",
                                  test_scope = "")
actionable_tmb_total_list <- get_reformatted_target_list(pancan_data_plus_vardata_variants, actionable_tmb_list)

tmb_cutoff <- 10
vardata_counts <- pancan_data_plus_vardata_variants %>% 
  select(public_sample_id, pan_cancer_code, somatic_coding_variants_per_mb) %>% 
  mutate(gene = ifelse(somatic_coding_variants_per_mb >= tmb_cutoff, "tmb", "no_tmb")) %>% 
  filter(gene == "tmb") %>% 
  count(public_sample_id, pan_cancer_code, gene) %>% 
  #this count will be counting total samples with 1+ variants in target, per disease type
  count(pan_cancer_code, gene) %>% 
  arrange(-n) %>% 
  left_join(actionable_tmb_total_list, ., by = c("pan_cancer_code" = "pan_cancer_code", "targets" = "gene")) %>% 
  left_join(., pancan_data_plus_vardata_variants %>% count(pan_cancer_code), by = c("pan_cancer_code" = "pan_cancer_code")) %>%
  mutate(percent = n.x/n.y*100) %>% 
  mutate(percent = case_when(is.na(percent) ~ 0,
                             percent <= 0.5 ~ percent,
                             percent > 0.5 ~ round(percent, 0)),
         n.x = ifelse(is.na(n.x), 0, n.x)) %>% 
  mutate(test_scope = factor(test_scope,
                             levels = c("gene_present_in_test_directory_for_disease",
                                        "gene_not_present_in_test_directory_for_disease"))) %>% 
  left_join(., pancan_data_plus_vardata_variants %>% 
              select(pan_cancer_code, pan_cancer_code_definition) %>% 
              unique(),
            by = c("pan_cancer_code" = "pan_cancer_code"))

source(paste0(filepaths$filepath_code, "get_pan_cancer_code_order.R"))
vardata_counts_levels <- get_vardata_counts_levels()
vardata_counts <- vardata_counts %>% slice(which(vardata_counts$pan_cancer_code_definition %in% vardata_counts_levels))
vardata_counts$pan_cancer_code_definition <- factor(vardata_counts$pan_cancer_code_definition,
                                                    levels = vardata_counts_levels)

source(paste0(filepaths$filepath_code, "get_vardata_counts_splits.R"))
vardata_counts <- get_vardata_counts_splits(vardata_counts)

source(paste0(filepaths$filepath_code, "get_actionability_plot.R"))
plot <- get_actionability_plot(vardata_counts)

#grab legend from plot...
p_legend <- gtable_filter(ggplot_gtable(ggplot_build(plot)), "guide-box")
#grab x-axis from plot...
p_xaxis <- gtable_filter(ggplot_gtable(ggplot_build(plot)), "axis-b")
#...then remove legend from plot
plot <- plot + theme(legend.position = "none",
                     axis.text.x = element_blank())

plot_width <- 0.21
labels_width <- 0.05
svglite(file = paste0(filepaths$filepath_plots, "pan_cancer_figure_2_tmb_", Sys.Date(),".svg"), height = 8, width = 12)
grid.newpage()
vp1 <- viewport(x = unit(0.0, "npc"), y = unit(0.18, "npc"), width = unit(plot_width, "npc"), height = unit(0.77, "npc"), just = c("left", "bottom"))
pushViewport(vp1)
grid.draw(ggplotGrob(plot))
popViewport()

#x-axis viewport
vp2 <- viewport(x = unit(0.167, "npc"), y = unit(0.15, "npc"), width = unit(labels_width, "npc"), height = unit(0.05, "npc"), just = c("left", "bottom"))
pushViewport(vp2)
grid.draw(p_xaxis)
popViewport()

dev.off()
